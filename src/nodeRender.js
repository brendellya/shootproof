'use strict';

export default function NodeRender() {
  const state = {
    target: document.querySelector('#app'),
    helpers: {
      sortNodes: (a, b) => {
        let aFirstSort = a['parent'];
        let bFirstSort = b['parent'];

        if (aFirstSort < bFirstSort) {
          return -1;
        } else if (bFirstSort === null) {
          return 1;
        } else if (aFirstSort === null) {
          return -1;
        }
      },
    }
  };

  function createElement(tag, options) {
        var $tag = document.createElement(tag);
        var attrs = (options && typeof options === 'object') ? Object.keys(options) : [];

        // Add node attributes
        attrs.forEach(function (attr) {
            $tag[attr] = options[attr];
        });
        return $tag;
      }

  function createNodeItem(node) {
    const $li = createElement('li', {
      className: (node.children.length)? 'expandable' : ''
    });
    const $div = createElement('div', { className: 'content'});
    const $pic = createElement('img', {
      className: 'pic',
      alt: node.thumbnail.description || 'cool pic!',
      src: node.thumbnail.href
    });
    const $name = createElement('span', {
      innerHTML: (node.children.length)? node.name + ' <i class="fa caret"></i>' : node.name
    });

    // Append Elements
    $div.appendChild($pic);
    $div.appendChild($name);
    $li.appendChild($div);

    if(node.children.length) {
      $div.addEventListener('click', function () {
        let parentNode = this.parentNode;
        let sub = parentNode.querySelector('.sub');
        let subState = sub.dataset.state;

        // Set expanded state
        if(!this.dataset.state){
          if(subState !== 'on') {
            sub.dataset.state = 'on';
            this.classList.add('show');
          } else {
            sub.dataset.state = 'off';
            this.classList.remove('show');
          }
        }
      }, false);
    }
    return $li;
  }

  function createNodeList(nodeList, parentNode){
    const $ul = createElement('ul', {
      className: (parentNode) ? 'sub' :'wrapper',
    });

    nodeList.forEach((node) => {
      const $node = createNodeItem(node); // li element

      // If sub nodes, transverse list
      if(node.children.length){
        const $subs = createNodeList(node.children, $node);
        $node.appendChild($subs);
      }

      $ul.appendChild($node);
    });
    return $ul;
  }

  function renderView(data) {
    const $list = createNodeList(data);
    state.target.appendChild($list);
  }

  function transformData(data) {
  // In case of the weird parent/child order
  const sortedData = data.sort(state.helpers.sortNodes);

  // Find any parents with children
  const findSubs = (parents, index) => parents.find((p) => p.id === index);
  const findParentNodes = (nodes) => nodes.reduce((memo, node) => {
    if(node.parent === null) {
      memo.push(Object.assign({
       children: []
      }, node))
    }
    return memo;
  }, []);
  const findChildNodes = (nodes, parents) => nodes.reduce((memo, node) => {
    memo = [...parents];

    if(node.parent !== null){
      let parentIndex = node.parent;
      let parent = findSubs(memo, parentIndex);
      node.children = [];

      //
      if(parent) {
        parent.children.push(node);
      } else {
        parents.forEach((p) => {
          if(p.children.length) {
            let subParent = findSubs(p.children, parentIndex);
            subParent.children.push(node);
          }
        });
      }
    }
    return memo;
  }, []);

  // Return nested array
  return findChildNodes(sortedData, findParentNodes(sortedData));
}

  function getNavData() {
    return fetch('testdata.json')
    .then((res) => res.json())
        .then((rawJson) => transformData(rawJson));
  }

  function init() {
    getNavData().then(function (data) {
      renderView(data);
    })
  }

  return {
    init
  }
}
